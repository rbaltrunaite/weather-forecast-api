let windowWidth = window.innerWidth;

if (windowWidth >= 576 && windowWidth <= 992) {
    let shorterWeekdaysNames = document.querySelectorAll(".weekDay");
    shorterWeekdaysNames.forEach(function(weekDay) {
        let shorterDay = weekDay.innerHTML.slice(0, 3);
        weekDay.innerHTML = shorterDay;
    });
}
