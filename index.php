<?php

    if(isset($_GET['city'])) {
        $city = $_GET['city'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.openweathermap.org/data/2.5/forecast?q={$_GET['city']}&units=metric&APPID=6f0b83dc522fa4b4def27150b4800432",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $characters = json_decode($response, true);
    }
    ?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/stylesheet.css">
    <title>Hello, world!</title>
</head>
<body>

<div class="container mt-5">
    <div class="container">
        <h1 class="">Weather forecast</h1>
        <form method="GET">
            <div class="form-group">
                <label for="city" class="h4 col-12 mx-auto">Enter the city</label>
                <div class="row">
                    <input type="text" class="form-control col-12 col-md-8 col-lg-6 my-2" name="city" id="city"
                           placeholder="e.g. Rome">
                    <div class="col-12 col-sm-4 my-2 px-auto">
                        <button type="submit" id="button" class="btn btn-primary">Let's see</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php if(isset($_GET['city'])): ?>
    <div class="container row mx-auto border mb-5 d-flex justify-content-center weather-result">
        <?php
        $chList = $characters['list'];
        $mainDescription = $chList[0]['weather'][0]['main'];

        $tempNow = $chList[0]['main']['temp'];
        $tempNow = intval($tempNow);
        $iconNow = $chList[0]['weather'][0]['icon'];

        $weekDays = array($chList);
        ?>
        <div class="row col-12 col-sm-12 col-md-6 col-lg-4 my-3">
            <div class="my-auto text-uppercase text-center col-12 col-sm-5 col-md-4">
                <p><strong><?php echo $city; ?></strong></p>
                <p>Weather</p>
            </div>
            <div class="col-12 row col-sm-7 col-md-8">
                <div class="my-auto col-6 col-sm-4 col-md-6 px-md-0">
                    <img src="assets/images/icons/<?php echo $iconNow; ?>.png" alt="weather icon">
                </div>
                <div class="text-center my-auto col-6 col-sm-8 col-md-6 px-md-0">
                    <h3><?php echo $tempNow; ?> &#8451;</h3>
                    <p><?php echo $mainDescription; ?></p>
                </div>
            </div>
        </div>
        <div class="row col-12 col-sm-10 col-md-6 col-lg-8 text-center">
        <?php
            $dateNow = $chList[0]['dt_txt'];
            $dateNow = (substr($dateNow, 0, 10));
            $n = 0;
            $i = 0;

            while ($i < 5):
                $n = $n + 1;
                $comingDate = substr($chList[$n]['dt_txt'], 0, 10);
                if ($dateNow < $comingDate):
                    $date = $chList[$n]['dt_txt'];
                    $weekDay = date("l", strtotime($date));

                    $icon = $chList[$n]['weather'][0]['icon'];

                    $tempMax = $chList[$n]['main']['temp_max'];
                    $tempMax = round($tempMax);
                    $tempMin = $chList[$n]['main']['temp_min'];
                    $tempMin = round($tempMin);

                    $dateNow = $comingDate;
                    $i += 1;

                    ;?>
            <div class="row col-12 col-sm-2 col-md-2 mx-auto px-0 weather-table">
                <div class="my-auto col-4 col-sm-12 px-0">
                    <h5 class="mt-1 px-auto weekDay"><?php echo $weekDay; ?></h5>
                </div>
                <div class="my-auto col-4 col-sm-12 px-0">
                    <img src="assets/images/icons/<?php echo $icon; ?>.png" class="w-100" alt="weather icon">
                </div>
                <div class="my-auto text-danger col-2 col-sm-12 col-lg-6 px-0" title="Day">
                    <p><?php echo$tempMax; ?> &#8451;</p>
                </div>
                <div class="my-auto text-primary col-2 col-sm-12 col-lg-6 px-0" title="Night">
                    <p><?php echo$tempMin; ?> &#8451;</p>
                </div>
            </div>
        <?php endif;
        endwhile;
        ?>
        </div>
    </div>
    <?php endif;?>
</div>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="assets/scripts/custom.js"></script>
</body>
</html>
